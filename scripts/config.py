from xAH_config import xAH_config
from ROOT import vector 
c = xAH_config()

triggers = ['L1_XE50', 'HLT_xe70', 'HLT_xe90_mht_L1XE50']
trigger_string = ",".join(triggers)

c.setalg("BasicEventSelection", {"m_name"                  : "BasicEventSelectionAlgo",
                                 "m_debug"                 : False,
                                 "m_isMC"                  : True,
                                 "m_truthLevelOnly"        : False,
                                 "m_applyGRLCut"           : False,
                                 "m_doPUreweighting"       : True,
                                  "m_lumiCalcFileNames"     :  "/afs/cern.ch/work/n/nwhallon/public/xAODAnaHelpers/MyxAH/R207/Test1/xAHmonoH/data/ilumicalc_histograms_None_266904-267639_periodA+B1_DetStatus-v62-pro18_DQDefects-00-01-02_PHYS_StandardGRL_All_Good.root",
                                  "m_PRWFileNames"          : "dev/PileupReweighting/mc15c_v2_defaults.NotRecommended.prw.root",
                                 "m_vertexContainerName"   : "PrimaryVertices",
                                 "m_PVNTrack"              : 2,
                                 "m_triggerSelection"      : trigger_string,
                            "m_applyPrimaryVertexCut" : True,
                            "m_applyEventCleaningCut" : True,
                            "m_applyTriggerCut"       : False,
                                 "m_useMetaData"           : False,
                                 "m_derivationName"        : ""
                                })

c.setalg("ElectronCalibrator", {"m_name"                : "ElectronCalibratorAlgo",
                                "m_inContainerName"     : "Electrons",
                                "m_outContainerName"    : "Electrons_Calib",
                                "m_inputAlgoSystNames"  : "",
                                "m_outputAlgoSystNames" : "ElectronCalibrator_Syst",
                                "m_esModel"             : "es2016data_mc15c",
                                "m_decorrelationModel"  : "1NP_v1"
                               })

c.setalg("TauSelector", {"m_name"                    : "TauSelectorAlgo",
                              "m_inContainerName"         : "TauJets",
                              "m_outContainerName"        : "preSelTauJets",
#                              "m_inputAlgoSystNames"      : "ElectronCalibrator_Syst",
#                              "m_outputAlgoSystNames"     : "ElectronSelectAlgo",
                              "m_decorateSelectedObjects" : True,
                              "m_createSelectedContainer" : True,
                              "m_ConfigPath" : "/afs/cern.ch/work/n/nwhallon/public/xAODAnaHelpers/MyxAH/R207/Test1/xAHmonoH/data/TauSelection.conf"
                             })

c.setalg("ElectronSelector", {"m_name"                    : "ElectronSelectorAlgo",
                              "m_inContainerName"         : "Electrons_Calib",
                              "m_outContainerName"        : "preSelElectrons",
                              "m_inputAlgoSystNames"      : "ElectronCalibrator_Syst",
                              "m_outputAlgoSystNames"     : "ElectronSelectAlgo",
                              "m_decorateSelectedObjects" : True,
                              "m_createSelectedContainer" : True,
                              "m_pT_min"                  : 10000,
                              "m_eta_max"                 : 2.47,
                              "m_vetoCrack"               : True,
                              "m_d0sig_max"               : 5.0,
                              "m_z0sintheta_max"          : 0.5,
                              "m_doLHPIDcut"              : True,
                              "m_LHOperatingPoint"        : "Tight",
                              "m_doCutBasedPIDcut"        : False,
                              "m_pass_min"                : 0,
                              "m_MinIsoWPCut"             : "Gradient",
                              "m_IsoWPList"               : "LooseTrackOnly,Loose,Tight,Gradient,GradientLoose"
                             })

c.setalg("MuonCalibrator", {"m_name"                : "MuonCalibratorAlgo",
                            "m_debug"               : False,
                            "m_release"             : "Recs2016_08_07",
                            "m_inContainerName"     : "Muons",
                            "m_outContainerName"    : "Muons_Calib",
                            "m_outputAlgoSystNames" : "MuonCalibrator_Syst",
                            "m_forceDataCalib"      : True,
                            "m_sort"                : True
                           })

c.setalg("MuonSelector", { "m_name"                 : "MuonSelectorAlgo",
                           "m_debug"                : False,
                           "m_pT_min"               : 10000,
                           "m_eta_max"              : 2.5,
                           "m_pass_min"             : 0,
                           "m_pass_max"             : 0,
                           "m_decorateSelectedObjects" : True,
                           "m_createSelectedContainer" : True,
                           "m_inContainerName"     : "Muons_Calib",
                           "m_outContainerName"    : "preSelMuons",
                           "m_inputAlgoSystNames"  : "MuonCalibrator_Syst",
                           "m_outputAlgoSystNames" : "MuonSelectAlgo",
                           "m_muonQualityStr"      : "Medium",
                           "m_d0sig_max"           : 3,
                           "m_z0sintheta_max"      : 0.5,
                           "m_MinIsoWPCut"         : "Gradient",
                           "m_IsoWPList"           : "LooseTrackOnly,Loose,Tight,Gradient,GradientLoose"
                           })

c.setalg("JetCalibrator", { "m_name"                  : "JetCalibrator_AntiKt4TopoEM",
                            "m_inContainerName"       : "AntiKt4EMTopoJets",
                            "m_outContainerName"      : "AntiKt4EMTopoJets_Calib",
                            "m_outputAlgo"            : "AntiKt4EMTopoJets_Calib_Algo",
                            "m_jetAlgo"               : "AntiKt4EMTopo",
                            "m_sort"                  : True,
                            "m_saveAllCleanDecisions" : True,
                            "m_calibConfigAFII"       : "JES_MC15Prerecommendation_AFII_June2015.config",
                            "m_calibConfigFullSim"    : "JES_data2016_data2015_Recommendation_Dec2016.config",
                            "m_calibConfigData"       : "JES_data2016_data2015_Recommendation_Dec2016.config",
                            "m_calibSequence"         : "JetArea_Residual_Origin_EtaJES_GSC",
                            "m_setAFII"               : False,
                            "m_JESUncertConfig"       : "JetUncertainties/JES_2016/Moriond2017/JES2016_SR_Scenario1.config",
                            "m_JESUncertMCType"       : "MC15",
                            "m_JERUncertConfig"       : "JetResolution/Prerec2015_xCalib_2012JER_ReducedTo9NP_Plots_v2.root",
                            "m_JERFullSys"            : False,
                            "m_JERApplyNominal"       : False,
#                            "m_redoJVT"               : True
                           })

c.setalg("JetSelector", { "m_name"                    :  "JetSelector_AntiKt4TopoEM",
                          "m_inContainerName"         :  "AntiKt4EMTopoJets_Calib",
                          "m_inputAlgo"               :  "AntiKt4EMTopoJets_Calib_Algo",
                          "m_outContainerName"        :  "preSelAntiKt4EMTopoJets_Calib",
                          "m_outputAlgo"              :  "preSelAntiKt4EMTopoJets_Calib_Algo",
                          "m_decorateSelectedObjects" :  False, 
                          "m_createSelectedContainer" :  True, 
                          "m_cleanJets"               :  True, 
                          "m_doJVT"                   :  True,
                          "m_cleanEvent"              :  True,
                          "m_pT_min"                  :  20e3,
                          "m_eta_max"                 :  2.5,
                          "m_useCutFlow"              :  True,
                          } )

c.setalg("JetCalibrator", {"m_name"                  : "JetCalibrator_AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets",
                           "m_inContainerName"       : "AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets",
                           "m_outContainerName"      : "AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets_JES",
                           "m_outputAlgo"            : "",
                           "m_jetAlgo"               : "AntiKt10LCTopoTrimmedPtFrac5SmallR20",
                           "m_sort"                  : True,
                           "m_saveAllCleanDecisions" : True,
                           "m_calibConfigFullSim"    : "JES_MC15recommendation_FatJet_Nov2016_QCDCombinationUncorrelatedWeights.config",
                           "m_calibConfigData"       : "JES_MC15recommendation_FatJet_Nov2016_QCDCombinationUncorrelatedWeights.config",
                           "m_doCleaning"            : False,
                           "m_JESUncertConfig"       : "JetUncertainties/UJ_2016/Moriond2017/UJ2016_CombinedMass_strong.config",
                           "m_JESUncertMCType"       : "MC15c",
                           "m_calibSequence"         : "EtaJES_JMS",
                           "m_setAFII"               : False,
                           "m_jetCleanCutLevel"      : "LooseBad",
                           "m_jetCleanUgly"          : True,
                           "m_cleanParent"           : True,
                           "m_applyFatJetPreSel"     : True,
                          })


c.setalg("JetSelector", {"m_name"                    : "JetSelector_AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets_JES",
                         "m_inContainerName"         : "AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets_JES",
                         "m_outContainerName"        : "preSelAntiKt10LCTopoTrimmedPtFrac5SmallR20Jets",
                         "m_inputAlgo"               : "",
                         "m_outputAlgo"              : "",
                         "m_decorateSelectedObjects" : False,
                         "m_createSelectedContainer" : True,
                         "m_cleanJets"               : True,
                         "m_pT_min"                  : 200e3,
                         "m_eta_max"                 : 2.0,
                         "m_mass_min"                : 0.1,
                         "m_useCutFlow"              : True,
                         "m_doJVF"                   : False
                        })
'''
c.setalg("BJetEfficiencyCorrector", { "m_name"                    : "BJetEfficiencyCorrector_AntiKt2PV0TrackJets",
                                      "m_inContainerName"         : "AntiKt2PV0TrackJets",
                                      "m_operatingPt"             : "FixedCutBEff_70", #FixedCutBEff_70
                                      "m_operatingPtCDI"          : "FixedCutBEff_70", #FixedCutBEff_70
                                      "m_corrFileName"            : "xAODBTaggingEfficiency/13TeV/2016-20_7-13TeV-MC15-CDI-2017-01-31_v1.root",
                                      "m_jetAuthor"               : "AntiKt2PV0TrackJets",
                                      "m_taggerName"              : "MV2c10",
                                      "m_decor"                   : "BTag",
                                     })
'''
c.setalg("JetSelector", { "m_name"                    : "JetSelector_AntiKt2PV0TrackJets",
                          "m_inContainerName"         : "AntiKt2PV0TrackJets",
                          "m_outContainerName"        : "preSelTrackJets",
                          "m_decorateSelectedObjects" : False,
                          "m_createSelectedContainer" : True,
                          "m_cleanJets"               : True,
                          "m_pT_min"                  : 10e3,
                          "m_eta_max"                 : 2.5,
                          "m_useCutFlow"              : True,
                          "m_doJVF"                   : False
                         })

c.setalg("METConstructor", {"m_name"                  : "METConstructorAlgo",
                            "m_debug"                 : False,
                            "m_referenceMETContainer" : "MET_Reference_AntiKt4EMTopo",
                            "m_mapName"               : "METAssoc_AntiKt4EMTopo",
                            "m_coreName"              : "MET_Core_AntiKt4EMTopo",
                            "m_outputContainer"	      : "RefFinalEM",
                            "m_outputAlgoSystNames"   : "",
                            "m_inputJets"             : "AntiKt4EMTopoJets_Calib",
                            "m_inputElectrons"        : "preSelElectrons",
                            "m_inputPhotons"          : "Photons",
                            "m_inputTaus"             : "preSelTauJets", # TauJets",
                            "m_inputMuons"            : "preSelMuons",
                            "m_doElectronCuts"        : False,
                            "m_doPhotonCuts"          : False,
                            "m_doTauCuts"             : False,
                            "m_doMuonCuts"            : False,
                            "m_doMuonEloss"           : True,
                            "m_doIsolMuonEloss"	      : True,
                           })

c.setalg("METConstructor", {"m_name"                  : "TrackMETConstructorAlgo",
                            "m_debug"                 : False,
                            "m_referenceMETContainer" : "MET_Reference_AntiKt4EMTopo",
                            "m_mapName"               : "METAssoc_AntiKt4EMTopo",
                            "m_coreName"              : "MET_Core_AntiKt4EMTopo",
                            "m_outputContainer"	      : "TrackRefFinalEM",
                            "m_outputAlgoSystNames"   : "",
                            "m_inputJets"             : "AntiKt4EMTopoJets_Calib",
                            "m_inputElectrons"        : "preSelElectrons",
                            "m_inputPhotons"          : "Photons",
                            "m_inputTaus"             : "preSelTauJets", # TauJets",
                            "m_inputMuons"            : "preSelMuons",
                            "m_doElectronCuts"        : False,
                            "m_doPhotonCuts"          : False,
                            "m_doTauCuts"             : False,
                            "m_doMuonCuts"            : False,
                            "m_doMuonEloss"           : True,
                            "m_doIsolMuonEloss"	      : True,
                            "m_rebuildUsingTracksInJets"   : True
                           })

c.setalg("xAHmonoHAlgo", {"m_name"               : "xAHmonoHAlgo",
                          "m_debug"              : False,
                          "m_tree_name"          : "tree",
                          "m_muonContainer_name" : "preSelMuons",
                          "m_electronContainer_name" : "preSelElectrons",
                          "m_smallJet_name" : "preSelAntiKt4EMTopoJets_Calib",
                          "m_trimmedFatJet_name" : "preSelAntiKt10LCTopoTrimmedPtFrac5SmallR20Jets",
                          "m_metContainer_name"  : "RefFinalEM",
                          "m_trackMetContainer_name"  : "TrackRefFinalEM",
                          "m_tauJet_name" : "preSelTauJets", # TauJets",
                          "m_trackJet_name" : "preSelTrackJets"
                         })
