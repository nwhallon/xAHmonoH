// EL include(s):
#include <EventLoop/Job.h>
#include <EventLoop/Worker.h>
#include <EventLoop/OutputStream.h>

// EDM include(s):
#include <xAODEventInfo/EventInfo.h>
#include <xAODJet/JetContainer.h>

// Add the truth container!
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthParticleContainer.h"

// package include(s):
#include <xAODAnaHelpers/HelperFunctions.h>
#include <xAODAnaHelpers/HelperClasses.h>
#include <xAODAnaHelpers/tools/ReturnCheck.h>
#include <xAODAnaHelpers/tools/ReturnCheckConfig.h>
#include <xAHmonoH/xAHmonoHAlgo.h>

// c++ include(s)
#include <stdexcept>

// this is needed to distribute the algorithm to the workers
ClassImp(xAHmonoHAlgo)


xAHmonoHAlgo :: xAHmonoHAlgo () :
  m_name(""),
  m_tree_name("tree"),
  m_muonContainer_name(""),
  m_electronContainer_name(""),
  m_smallJet_name(""),
  m_trimmedFatJet_name(""),
  m_metContainer_name(""),
  m_trackMetContainer_name(""),
  m_tauJet_name(""),
  m_trackJet_name("")
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
  msg().setName( m_name );

  ATH_MSG_INFO( "Calling constructor");
}

EL::StatusCode xAHmonoHAlgo :: setupJob (EL::Job& job)
{
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.

  job.useXAOD();
  xAOD::Init( "xAHmonoHAlgo" ).ignore(); // call before opening first file

  EL::OutputStream outForTree("tree");
  job.outputAdd (outForTree);

  return EL::StatusCode::SUCCESS;

}

EL::StatusCode xAHmonoHAlgo :: histInitialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.
  ATH_MSG_INFO( "Calling histInitialize");

  // Number of events
  h_EventCounter = new TH1D("h_EventCounter","h_EventCounter",10,0,10);
  wk()->addOutput (h_EventCounter);

  //////////////////////////
  // Histograms
  //////////////////////////
  h_cutflow = new TH1D("h_cutflow", "h_cutflow", 20, 0, 20);
  h_cutflow_resolved = new TH1D("h_cutflow_resolved", "h_cutflow_resolved", 20, 0, 20);
  h_trimmedFatJet_pt = new TH1D("h_trimmedFatJet_pt", "h_trimmedFatJet_pt",100,0,1000);

  // Add them so they automatically appear in output file
  wk()->addOutput (h_cutflow);
  wk()->addOutput (h_cutflow_resolved);

  //////////////////////////
  // Tree
  // You need to do this process to link the tree to the output
  //////////////////////////
  //std::cout<<"Trying to initialize tree stuff"<<std::endl;
  // get the file we created already
  // get the file we created already
  TFile* treeFile = wk()->getOutputFile ("tree");
  treeFile->mkdir("TreeDirectory");
  treeFile->cd("TreeDirectory");

  outTree = new TTree(m_tree_name.c_str(),m_tree_name.c_str());

  outTree->Branch("tvar_cutflow_resolved_init",    &tvar_cutflow_resolved_init);
  outTree->Branch("tvar_cutflow_resolved_lepton",    &tvar_cutflow_resolved_lepton);
  outTree->Branch("tvar_cutflow_resolved_met_lt500",    &tvar_cutflow_resolved_met_lt500);
  outTree->Branch("tvar_cutflow_resolved_met_gt150",    &tvar_cutflow_resolved_met_gt150);
  outTree->Branch("tvar_cutflow_resolved_mpt_gt30",    &tvar_cutflow_resolved_mpt_gt30);
  outTree->Branch("tvar_cutflow_resolved_mindphi_gt20",    &tvar_cutflow_resolved_mindphi_gt20);
  outTree->Branch("tvar_cutflow_resolved_dphimetmpt_lt90",    &tvar_cutflow_resolved_dphimetmpt_lt90);
  outTree->Branch("tvar_cutflow_resolved_njetsCentral_gt2",    &tvar_cutflow_resolved_njetsCentral_gt2);

  outTree->Branch("tvar_cutflow_merged_init",    &tvar_cutflow_merged_init);
  outTree->Branch("tvar_cutflow_merged_lepton",    &tvar_cutflow_merged_lepton);
  outTree->Branch("tvar_cutflow_merged_met_gt500",    &tvar_cutflow_merged_met_gt500);
  outTree->Branch("tvar_cutflow_merged_mpt_gt30",    &tvar_cutflow_merged_mpt_gt30);
  outTree->Branch("tvar_cutflow_merged_mindphi_gt20",    &tvar_cutflow_merged_mindphi_gt20);
  outTree->Branch("tvar_cutflow_merged_dphimetmpt_lt90",    &tvar_cutflow_merged_dphimetmpt_lt90);
  outTree->Branch("tvar_cutflow_merged_nfatjet_gt1",    &tvar_cutflow_merged_nfatjet_gt1);
  outTree->Branch("tvar_cutflow_merged_taus",    &tvar_cutflow_merged_taus);
  outTree->Branch("tvar_cutflow_merged_bJetVeto", &tvar_cutflow_merged_bJetVeto);
  outTree->Branch("tvar_cutflow_merged_HTcut", &tvar_cutflow_merged_HTcut);
  outTree->Branch("tvar_cutflow_merged_0btag",    &tvar_cutflow_merged_0btag);
  outTree->Branch("tvar_cutflow_merged_1btag",    &tvar_cutflow_merged_1btag);
  outTree->Branch("tvar_cutflow_merged_2btag",    &tvar_cutflow_merged_2btag);
  outTree->Branch("tvar_muon_num",    &tvar_muon_num);
  outTree->Branch("tvar_electron_num",    &tvar_electron_num);
  outTree->Branch("tvar_smallJet_btag_num", &tvar_smallJet_btag_num);
  outTree->Branch("tvar_ghostAssociatedTrackJet_btag_num", &tvar_ghostAssociatedTrackJet_btag_num);
  outTree->Branch("tvar_centralJet_pt", &tvar_centralJet_pt);
  outTree->Branch("tvar_centralJet_eta", &tvar_centralJet_eta);
  outTree->Branch("tvar_centralJet_phi", &tvar_centralJet_phi);
  outTree->Branch("tvar_centralJet_m", &tvar_centralJet_m);
  outTree->Branch("tvar_forwardJet_pt", &tvar_forwardJet_pt);
  outTree->Branch("tvar_forwardJet_eta", &tvar_forwardJet_eta);
  outTree->Branch("tvar_forwardJet_phi", &tvar_forwardJet_phi);
  outTree->Branch("tvar_forwardJet_m", &tvar_forwardJet_m);
  outTree->Branch("tvar_trimmedFatJet_pt",    &tvar_trimmedFatJet_pt);
  outTree->Branch("tvar_met_FinalTrk_met",    &tvar_met_FinalTrk_met);
  outTree->Branch("tvar_met_FinalTrk_phi",    &tvar_met_FinalTrk_phi);
  outTree->Branch("tvar_met_RefJetTrk_met",    &tvar_met_RefJetTrk_met);
  outTree->Branch("tvar_met_RefJetTrk_phi",    &tvar_met_RefJetTrk_phi);

  //add the TTree to the output
  outTree->SetDirectory( treeFile->GetDirectory("TreeDirectory") );

  return EL::StatusCode::SUCCESS;

}

EL::StatusCode xAHmonoHAlgo :: fileExecute ()
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed


  m_event = wk()->xaodEvent();
  // get the MetaData tree once a new file is opened, with
  TTree *MetaData = dynamic_cast<TTree*>(wk()->inputFile()->Get("MetaData"));
  if (!MetaData) {
    ATH_MSG_ERROR( "MetaData not found! Exiting.");
    return EL::StatusCode::FAILURE;
  }
  MetaData->LoadTree(0);
  //check if file is from a DxAOD
  bool m_isDerivation = !MetaData->GetBranch("StreamAOD");

  if(m_isDerivation ){
    // check for corruption
    const xAOD::CutBookkeeperContainer* incompleteCBC = nullptr;
    if(!m_event->retrieveMetaInput(incompleteCBC, "IncompleteCutBookkeepers").isSuccess()){
      ATH_MSG_WARNING("Failed to retrieve IncompleteCutBookkeepers from MetaData! Exiting.");
      //return EL::StatusCode::FAILURE;
    }
    if ( incompleteCBC->size() != 0 ) {
      ATH_MSG_WARNING("Found incomplete Bookkeepers! Check file for corruption.");
      //return EL::StatusCode::FAILURE;
    }

    // Now, let's find the actual information
    const xAOD::CutBookkeeperContainer* completeCBC = 0;
    if(!m_event->retrieveMetaInput(completeCBC, "CutBookkeepers").isSuccess()){
      ATH_MSG_WARNING("Failed to retrieve CutBookkeepers from MetaData! Exiting.");
      //return EL::StatusCode::FAILURE;
    }
    // Now, let's actually find the right one that contains all the needed info...
    const xAOD::CutBookkeeper* allEventsCBK = 0;
    int maxCycle = -1;
    for (const auto& cbk: *completeCBC) {
      if (cbk->cycle() > maxCycle && cbk->name() == "AllExecutedEvents" && cbk->inputStream() == "StreamAOD") {
        allEventsCBK = cbk;
        maxCycle = cbk->cycle();
      }
    }
    uint64_t nEventsProcessed  = allEventsCBK->nAcceptedEvents();
    double sumOfWeights        = allEventsCBK->sumOfEventWeights();
    double sumOfWeightsSquared = allEventsCBK->sumOfEventWeightsSquared();

    h_EventCounter->Fill(0.0,(int)nEventsProcessed);
    h_EventCounter->Fill(1.0,sumOfWeights);
    h_EventCounter->Fill(2.0,sumOfWeightsSquared);
  }

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode xAHmonoHAlgo :: changeInput (bool /*firstFile*/)
{
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.

  return EL::StatusCode::SUCCESS;

}

EL::StatusCode xAHmonoHAlgo :: initialize ()
{
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the outputif you have no
  // input events.

  ATH_MSG_INFO( m_name.c_str());
  m_event = wk()->xaodEvent();
  m_store = wk()->xaodStore();

  // count number of events
  m_eventCounter = 0;

  ATH_MSG_INFO( "xAHmonoHAlgo Interface succesfully initialized!" );
  return EL::StatusCode::SUCCESS;
}


EL::StatusCode xAHmonoHAlgo :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  if ( m_debug ) { ATH_MSG_INFO( "Executing xAHmonoHAlgo..."); }

  if ( (m_eventCounter % 5000) == 0 ) {
    ATH_MSG_INFO( m_eventCounter );
  }

  ++m_eventCounter;

  // initialize tree variable values
  tvar_cutflow_resolved_init = 0;
  tvar_cutflow_resolved_lepton = 0;
  tvar_cutflow_resolved_met_lt500 = 0;
  tvar_cutflow_resolved_met_gt150 = 0;
  tvar_cutflow_resolved_mpt_gt30 = 0;
  tvar_cutflow_resolved_mindphi_gt20 = 0;
  tvar_cutflow_resolved_dphimetmpt_lt90 = 0;
  tvar_cutflow_resolved_njetsCentral_gt2 = 0;

  tvar_cutflow_merged_init = 0;
  tvar_cutflow_merged_lepton = 0;
  tvar_cutflow_merged_met_gt500 = 0;
  tvar_cutflow_merged_mpt_gt30 = 0;
  tvar_cutflow_merged_mindphi_gt20 = 0;
  tvar_cutflow_merged_dphimetmpt_lt90 = 0;
  tvar_cutflow_merged_nfatjet_gt1 = 0;
  tvar_cutflow_merged_taus = 0;
  tvar_cutflow_merged_bJetVeto = 0;
  tvar_cutflow_merged_HTcut = 0;
  tvar_cutflow_merged_0btag = 0;
  tvar_cutflow_merged_1btag = 0;
  tvar_cutflow_merged_2btag = 0;

  bool resolvedPassedPreviousCut = 1;
  bool mergedPassedPreviousCut = 1;

  const xAOD::EventInfo* eventInfo(nullptr);
  RETURN_CHECK("xAHmonoHAlgo::execute()", HelperFunctions::retrieve(eventInfo, "EventInfo", m_event, m_store), "");

  // event information useful for debugging
//  if (eventInfo->eventNumber() != 658) return EL::StatusCode::SUCCESS; // debug
//  printf("event number = %d\n", eventInfo->eventNumber()); // debug

  // Get Objects -------------------------------------------------------------------------------------------------------------------------------------------------
  // Retrieve the container of primary vertices
  const xAOD::VertexContainer* vertices(0);
  if(m_debug) std::cout << " Getting primary vertices: " << " PrimaryVertices" << std::endl;
  RETURN_CHECK("xAHmonoHAlgo::execute()", HelperFunctions::retrieve(vertices, "PrimaryVertices", m_event, m_store), "");

  const xAOD::Vertex *pv = 0;
  pv = vertices->at( HelperFunctions::getPrimaryVertexLocation(vertices) );

  // Retrieve the container of trimmed fat jets
  const xAOD::JetContainer* trimmedFatJets(0);
  if(m_debug) std::cout << " Getting fat jets: "  << m_trimmedFatJet_name << std::endl;
  RETURN_CHECK("xAHmonoHAlgo::execute()", HelperFunctions::retrieve(trimmedFatJets, m_trimmedFatJet_name, m_event, m_store), "");

  const xAOD::Jet* leadingTrimmedFatJet(0);
  for(auto trimmedFatJet : *trimmedFatJets) {
    leadingTrimmedFatJet = trimmedFatJet;
    break;
  }

  for(auto trimmedFatJet : *trimmedFatJets) {
    tvar_trimmedFatJet_pt.push_back(trimmedFatJet->pt());
  }

 // this is a suuuuuper bizarre bug that needs investigation
//  if (tvar_trimmedFatJet_pt.size() != trimmedFatJets->size()) printf("NIKOLA: THIS IS MESSED UP, %d, %d\n", trimmedFatJets->size(), tvar_trimmedFatJet_pt.size());

  // Retrieve the container of Muons
  const xAOD::MuonContainer* muons(nullptr);
  if(m_debug) std::cout << " Getting muons: "  << m_muonContainer_name << std::endl;
  ANA_CHECK( HelperFunctions::retrieve(muons, m_muonContainer_name, m_event, m_store) );

  tvar_muon_num = muons->size();

  // Retrieve the container of Electrons
  const xAOD::ElectronContainer* electrons(nullptr);
  if(m_debug) std::cout << " Getting electrons: "  << m_electronContainer_name << std::endl;
  ANA_CHECK( HelperFunctions::retrieve(electrons, m_electronContainer_name, m_event, m_store) );

  tvar_electron_num = electrons->size();

  // Retrieve the container of MET
  const xAOD::MissingETContainer* metContainer(0);
  if(m_debug) std::cout << "Getting MET: " << m_metContainer_name << std::endl;
  RETURN_CHECK("xAHmonoHAlgo::execute()", HelperFunctions::retrieve(metContainer, m_metContainer_name, m_event, m_store), "");

  const xAOD::MissingET* met_FinalTrk = *metContainer->find("FinalTrk");
  tvar_met_FinalTrk_met = met_FinalTrk->met();
  tvar_met_FinalTrk_phi = met_FinalTrk->phi();

  // track met is now working
  const xAOD::MissingETContainer* trackMetContainer(0);
  if(m_debug) std::cout << "Getting Track MET: " << m_trackMetContainer_name << std::endl;
  RETURN_CHECK("xAHmonoHAlgo::execute()", HelperFunctions::retrieve(trackMetContainer, m_trackMetContainer_name, m_event, m_store), "");

  const xAOD::MissingET* met_RefJetTrk = *trackMetContainer->find("RefJetTrk");
  tvar_met_RefJetTrk_met = met_RefJetTrk->met();
  tvar_met_RefJetTrk_phi = met_RefJetTrk->phi();

  // Retrieve the container of track jets
  std::vector<const xAOD::Jet*> allTrackJets;
  const xAOD::JetContainer* trackJets(0);
  if(m_debug) std::cout << " Getting track jets: "  << m_trackJet_name << std::endl;
  RETURN_CHECK("xAHmonoHAlgo::execute()", HelperFunctions::retrieve(trackJets, m_trackJet_name, m_event, m_store), "");

  // apply selection to track jets
  for (int i = 0; i < trackJets->size(); i++) {
    if (trackJets->at(i)->pt() < 10000) continue;
    if (fabs(trackJets->at(i)->eta()) > 2.5) continue;
    if (trackJets->at(i)->numConstituents() < 2) continue;
    allTrackJets.push_back(trackJets->at(i));
  }

  // Get the ghost associated track jets
  std::vector<const xAOD::Jet*> ghostAssociatedTrackJets;
  if (leadingTrimmedFatJet) {
    ElementLink<xAOD::JetContainer> leadingTrimmedFatJetParent_el = leadingTrimmedFatJet->auxdata<ElementLink<xAOD::JetContainer> >("Parent");
    if (leadingTrimmedFatJetParent_el.isValid()) {
      (*leadingTrimmedFatJetParent_el)->getAssociatedObjects<xAOD::Jet>("GhostAntiKt2TrackJet", ghostAssociatedTrackJets);
    }
  }

  // apply selection to ghost associated track jets
  std::vector<bool> eraseVector;
  for (int i = 0; i < ghostAssociatedTrackJets.size(); i++) {
    bool shouldErase = 0;
    if (ghostAssociatedTrackJets.at(i)->pt() < 10000) shouldErase = 1;
    if (fabs(ghostAssociatedTrackJets.at(i)->eta()) > 2.5) shouldErase = 1;
    if (ghostAssociatedTrackJets.at(i)->numConstituents() < 2) shouldErase = 1;
    eraseVector.push_back(shouldErase);
  }
  for (int i = eraseVector.size() - 1; i >= 0; i--) {
    if (eraseVector.at(i) == 1) ghostAssociatedTrackJets.erase(ghostAssociatedTrackJets.begin() + i);
  }

  // Get the cone non-associated track jets
  std::vector<const xAOD::Jet*> coneNonAssociatedTrackJets;
  std::vector<const xAOD::Jet*> btaggedConeNonAssociatedTrackJets;
  if (leadingTrimmedFatJet) {
    for (auto trackJet : allTrackJets) {
      if (trackJet->p4().DeltaR(leadingTrimmedFatJet->p4()) > 1.0) {
        coneNonAssociatedTrackJets.push_back(trackJet);
        if (trackJet->btagging()->auxdata<double>("MV2c10_discriminant") > 0.6455) btaggedConeNonAssociatedTrackJets.push_back(trackJet);
      }
    }
  }

  // Get the b-tagging information for the ghost associated track jets
  tvar_ghostAssociatedTrackJet_btag_num = 0;
  int i = 0;
  for(auto trackJet : ghostAssociatedTrackJets) {
    if (i == 2) break; // this ensures we only look at the leading 2 associated track jets
    if (trackJet->btagging()->auxdata<double>("MV2c10_discriminant") > 0.6455) tvar_ghostAssociatedTrackJet_btag_num++; // https://twiki.cern.ch/twiki/bin/view/AtlasProtected/BTaggingBenchmarksRelease20#AntiKt2PV0TrackJets
    i++;
  }

  // Retrieve the container of small jets
  const xAOD::JetContainer* smallJets(0);
  if(m_debug) std::cout << " Getting small jets: "  << m_smallJet_name << std::endl;
  RETURN_CHECK("xAHmonoHAlgo::execute()", HelperFunctions::retrieve(smallJets, m_smallJet_name, m_event, m_store), "");

  std::vector<const xAOD::Jet*> centralJets;
  std::vector<const xAOD::Jet*> btaggedCentralJets;
  std::vector<const xAOD::Jet*> nonBtaggedCentralJets;
  std::vector<const xAOD::Jet*> forwardJets;
  tvar_smallJet_btag_num = 0;
  for (auto smallJet : *smallJets) {
    if (fabs(smallJet->eta()) < 2.5) {
      centralJets.push_back(smallJet);
      if (smallJet->btagging()->auxdata<double>("MV2c10_discriminant") > 0.8244273) {
        tvar_smallJet_btag_num++; // https://twiki.cern.ch/twiki/bin/view/AtlasProtected/BTaggingBenchmarksRelease20#AntiKt4EMTopoJets
        btaggedCentralJets.push_back(smallJet);
      }
      else {
       nonBtaggedCentralJets.push_back(smallJet);
      }
      tvar_centralJet_pt.push_back(smallJet->pt());
      tvar_centralJet_eta.push_back(smallJet->eta());
      tvar_centralJet_phi.push_back(smallJet->phi());
      tvar_centralJet_m.push_back(smallJet->m());
    }
    else {
      forwardJets.push_back(smallJet);
      if (smallJet->btagging()->auxdata<double>("MV2c10_discriminant") > 0.8244273) tvar_smallJet_btag_num++; // https://twiki.cern.ch/twiki/bin/view/AtlasProtected/BTaggingBenchmarksRelease20#AntiKt4EMTopoJets
      tvar_forwardJet_pt.push_back(smallJet->pt());
      tvar_forwardJet_eta.push_back(smallJet->eta());
      tvar_forwardJet_phi.push_back(smallJet->phi());
      tvar_forwardJet_m.push_back(smallJet->m());
    }
  }

  std::vector<const xAOD::Jet*> btaggedPlusNonBtaggedPlusForwardJets;
  for (int i = 0; i < btaggedCentralJets.size(); i++) btaggedPlusNonBtaggedPlusForwardJets.push_back(btaggedCentralJets.at(i));
  for (int i = 0; i < nonBtaggedCentralJets.size(); i++) btaggedPlusNonBtaggedPlusForwardJets.push_back(nonBtaggedCentralJets.at(i));
  for (int i = 0; i < forwardJets.size(); i++) btaggedPlusNonBtaggedPlusForwardJets.push_back(forwardJets.at(i));

  std::vector<const xAOD::Jet*> centralPlusForwardJets;
  for (int i = 0; i < centralJets.size(); i++) centralPlusForwardJets.push_back(centralJets.at(i));
  for (int i = 0; i < forwardJets.size(); i++) centralPlusForwardJets.push_back(forwardJets.at(i));

  std::vector<float> centralPlusForwardJet_phi;
  for (int i = 0; i < tvar_centralJet_phi.size(); i++) centralPlusForwardJet_phi.push_back(tvar_centralJet_phi.at(i));
  for (int i = 0; i < tvar_forwardJet_phi.size(); i++) centralPlusForwardJet_phi.push_back(tvar_forwardJet_phi.at(i));

  // Retrieve the container of tau jets
  const xAOD::TauJetContainer* tauJets(0);
  if(m_debug) std::cout << " Getting tau jets: "  << m_tauJet_name << std::endl;
  RETURN_CHECK("xAHmonoHAlgo::execute()", HelperFunctions::retrieve(tauJets, m_tauJet_name, m_event, m_store), "");

  // cutflow
  if (!pv) resolvedPassedPreviousCut = 0;
  if (resolvedPassedPreviousCut) {
    h_cutflow_resolved->Fill(0);
    tvar_cutflow_resolved_init = 1;
    resolvedPassedPreviousCut = 1;
  }

  // cutflow
  if (tvar_electron_num + tvar_muon_num > 0) resolvedPassedPreviousCut = 0; // return EL::StatusCode::SUCCESS;
  if (resolvedPassedPreviousCut) {
    h_cutflow_resolved->Fill(1);
    tvar_cutflow_resolved_lepton = 1;
    resolvedPassedPreviousCut = 1;
  }

  // cutflow
  if (tvar_met_FinalTrk_met > 500000) resolvedPassedPreviousCut = 0; // return EL::StatusCode::SUCCESS;
  if (resolvedPassedPreviousCut) {
    h_cutflow_resolved->Fill(2);
    tvar_cutflow_resolved_met_lt500 = 1;
    resolvedPassedPreviousCut = 1;
  }

  // cutflow
  if (tvar_met_FinalTrk_met < 150000) resolvedPassedPreviousCut = 0; // return EL::StatusCode::SUCCESS;
  if (resolvedPassedPreviousCut) {
    h_cutflow_resolved->Fill(3);
    tvar_cutflow_resolved_met_gt150 = 1;
    resolvedPassedPreviousCut = 1;
  }

  // cutflow
  if (tvar_met_RefJetTrk_met < 30000) resolvedPassedPreviousCut = 0; // return EL::StatusCode::SUCCESS;
  if (resolvedPassedPreviousCut) {
    h_cutflow_resolved->Fill(4);
    tvar_cutflow_resolved_mpt_gt30 = 1;
    resolvedPassedPreviousCut = 1;
  }
{
  // cutflow
  float min_dphi_met_centralPlusForwardJet = 1.0;
  for (int i = 0; i < centralPlusForwardJet_phi.size(); i++) {
    if (i == 3) break;
    float dphi = fabs(tvar_met_FinalTrk_phi - centralPlusForwardJet_phi.at(i));
    if (dphi > 3.14159265359) dphi = 2 * 3.14159265359 - dphi;
    if (dphi < min_dphi_met_centralPlusForwardJet) {
      min_dphi_met_centralPlusForwardJet = dphi;
    }
  }
  if (min_dphi_met_centralPlusForwardJet < 0.349066) resolvedPassedPreviousCut = 0; // return EL::StatusCode::SUCCESS;
  if (resolvedPassedPreviousCut) {
    h_cutflow_resolved->Fill(5);
    tvar_cutflow_resolved_mindphi_gt20 = 1;
    resolvedPassedPreviousCut = 1;
  }
}
{
  // cutflow
  float dphimetmpt = fabs(tvar_met_FinalTrk_phi - tvar_met_RefJetTrk_phi);
  if (dphimetmpt > 3.14159265359) dphimetmpt = 2 * 3.14159265359 - dphimetmpt;
  if (dphimetmpt > 1.5708) resolvedPassedPreviousCut = 0;
  if (resolvedPassedPreviousCut) {
    h_cutflow_resolved->Fill(6);
    tvar_cutflow_resolved_dphimetmpt_lt90 = 1;
    resolvedPassedPreviousCut = 1;
  }
}

  // cutflow
  if (tvar_centralJet_pt.size() < 2) resolvedPassedPreviousCut = 0;
  if (resolvedPassedPreviousCut) {
    h_cutflow_resolved->Fill(7);
    tvar_cutflow_resolved_njetsCentral_gt2 = 1;
    resolvedPassedPreviousCut = 1;
  }

  // cutflow
  if (tvar_centralJet_pt.size() >= 2) {
    if (btaggedPlusNonBtaggedPlusForwardJets.at(0)->pt() < 45000 && btaggedPlusNonBtaggedPlusForwardJets.at(1)->pt() < 45000) resolvedPassedPreviousCut = 0;
  }
  if (resolvedPassedPreviousCut) {
    h_cutflow_resolved->Fill(8); // I'm getting lazy
  }

  if (tvar_centralJet_pt.size() >= 2) {
    if (centralPlusForwardJets.size() == 2) {
      if (centralPlusForwardJets.at(0)->pt() + centralPlusForwardJets.at(1)->pt() < 120) resolvedPassedPreviousCut = 0;
    }
    else if (centralPlusForwardJets.size() == 3) {
      if (centralPlusForwardJets.at(0)->pt() + centralPlusForwardJets.at(1)->pt() + centralPlusForwardJets.at(2)->pt() < 150) resolvedPassedPreviousCut = 0;
    }
  }
  if (resolvedPassedPreviousCut) {
    h_cutflow_resolved->Fill(9); // I'm getting lazy
  }

  // cutflow
  if (tvar_centralJet_pt.size() >= 2) {
    if (btaggedPlusNonBtaggedPlusForwardJets.at(0)->p4().DeltaPhi(btaggedPlusNonBtaggedPlusForwardJets.at(1)->p4()) > 2.44346) resolvedPassedPreviousCut = 0;
  }
  if (resolvedPassedPreviousCut) {
    h_cutflow_resolved->Fill(10);
  }

  // cutflow
  if (tvar_centralJet_pt.size() >= 2) {
    float dphi = fabs(tvar_met_FinalTrk_phi - (btaggedPlusNonBtaggedPlusForwardJets.at(0)->p4() + btaggedPlusNonBtaggedPlusForwardJets.at(1)->p4()).Phi());
    if (dphi > 3.14159265359) dphi = 2 * 3.14159265359 - dphi;
    if (dphi < 2.0944) resolvedPassedPreviousCut = 0;
  }
  if (resolvedPassedPreviousCut) {
    h_cutflow_resolved->Fill(11);
  }

  // cutflow
  for(auto tauJet : *tauJets) {
    if (tauJet->isTau(xAOD::TauJetParameters::JetBDTSigLoose)) resolvedPassedPreviousCut = 0;
  }
  if (resolvedPassedPreviousCut) {
    h_cutflow_resolved->Fill(12);
  }

  // cutflow
  if (tvar_centralJet_pt.size() >= 2) {
    if (btaggedPlusNonBtaggedPlusForwardJets.at(0)->p4().DeltaR(btaggedPlusNonBtaggedPlusForwardJets.at(1)->p4()) > 1.8) resolvedPassedPreviousCut = 0;
  }
  if (resolvedPassedPreviousCut) {
    h_cutflow_resolved->Fill(13);
  }

  // cutflow
  if (btaggedCentralJets.size() > 2) resolvedPassedPreviousCut = 0; // might need to change this to check inclusive central jets
  if (resolvedPassedPreviousCut) h_cutflow_resolved->Fill(14);

  // cutflow
{
  float HT = 0;
  for (auto smallJet : *smallJets) HT += smallJet->pt();
  float ptj1 = 0;
  float ptj2 = 0;
  float ptj3 = 0;
  if (btaggedPlusNonBtaggedPlusForwardJets.size() > 0) ptj1 = btaggedPlusNonBtaggedPlusForwardJets.at(0)->pt();
  if (btaggedPlusNonBtaggedPlusForwardJets.size() > 1) ptj2 = btaggedPlusNonBtaggedPlusForwardJets.at(1)->pt();
  if (btaggedPlusNonBtaggedPlusForwardJets.size() > 2) ptj3 = btaggedPlusNonBtaggedPlusForwardJets.at(2)->pt();
  float HT4 = HT - (ptj1 + ptj2 + ptj3);
  if (HT4 / HT > 0.37) resolvedPassedPreviousCut = 0;
  if (resolvedPassedPreviousCut) h_cutflow_resolved->Fill(15);
}

  // cutflow
  if (resolvedPassedPreviousCut && btaggedCentralJets.size() == 0) h_cutflow_resolved->Fill(16);
  if (resolvedPassedPreviousCut && btaggedCentralJets.size() == 1) h_cutflow_resolved->Fill(17);
  if (resolvedPassedPreviousCut && btaggedCentralJets.size() == 2) h_cutflow_resolved->Fill(18);

  // Apply Cutflow -------------------------------------------------------------------------------------------------------------------------------------------------
  // cutflow
  if (!pv) mergedPassedPreviousCut = 0;
  if (mergedPassedPreviousCut) {
    h_cutflow->Fill(0);
    tvar_cutflow_merged_init = 1;
    mergedPassedPreviousCut = 1;
  }

  // cutflow
  if (tvar_electron_num + tvar_muon_num > 0) mergedPassedPreviousCut = 0; // return EL::StatusCode::SUCCESS;
  if (mergedPassedPreviousCut) {
    h_cutflow->Fill(1);
    tvar_cutflow_merged_lepton = 1;
    mergedPassedPreviousCut = 1;
  }

  // cutflow
  if (tvar_met_FinalTrk_met < 500000) mergedPassedPreviousCut = 0; // return EL::StatusCode::SUCCESS;
  if (mergedPassedPreviousCut) {
    h_cutflow->Fill(2);
    tvar_cutflow_merged_met_gt500 = 1;
    mergedPassedPreviousCut = 1;
  }

  // cutflow
  if (tvar_ghostAssociatedTrackJet_btag_num < 2 && tvar_met_RefJetTrk_met < 30000) mergedPassedPreviousCut = 0;
  if (mergedPassedPreviousCut) {
    h_cutflow->Fill(3);
    tvar_cutflow_merged_mpt_gt30 = 1;
    mergedPassedPreviousCut = 1;
  }
{
  // cutflow
  float min_dphi_met_centralPlusForwardJet = 1.0;
  for (int i = 0; i < centralPlusForwardJet_phi.size(); i++) {
    if (i == 3) break;
    float dphi = fabs(tvar_met_FinalTrk_phi - centralPlusForwardJet_phi.at(i));
    if (dphi > 3.14159265359) dphi = 2 * 3.14159265359 - dphi;
    if (dphi < min_dphi_met_centralPlusForwardJet) {
      min_dphi_met_centralPlusForwardJet = dphi;
    }
  }
  if (min_dphi_met_centralPlusForwardJet < 0.349066) mergedPassedPreviousCut = 0; // return EL::StatusCode::SUCCESS;
  if (mergedPassedPreviousCut) {
    h_cutflow->Fill(4);
    tvar_cutflow_merged_mindphi_gt20 = 1;
    mergedPassedPreviousCut = 1;
  }
}
{
  // cutflow
  float dphimetmpt = fabs(tvar_met_FinalTrk_phi - tvar_met_RefJetTrk_phi);
  if (dphimetmpt > 3.14159265359) dphimetmpt = 2 * 3.14159265359 - dphimetmpt;
  if (dphimetmpt > 1.5708) mergedPassedPreviousCut = 0;
  if (mergedPassedPreviousCut) {
    h_cutflow->Fill(5);
    tvar_cutflow_merged_dphimetmpt_lt90 = 1;
    mergedPassedPreviousCut = 1;
  }
}
  // cutflow
  if (!leadingTrimmedFatJet) mergedPassedPreviousCut = 0; // return EL::StatusCode::SUCCESS;
  if (mergedPassedPreviousCut) {
    h_cutflow->Fill(6);
    tvar_cutflow_merged_nfatjet_gt1 = 1;
    mergedPassedPreviousCut = 1;
  }

  // cutflow
  for(auto tauJet : *tauJets) {
    if (tauJet->isTau(xAOD::TauJetParameters::JetBDTSigLoose)) {
      if (leadingTrimmedFatJet) {
        if (tauJet->p4().DeltaR(leadingTrimmedFatJet->p4()) >= 1.0) mergedPassedPreviousCut = 0; // return EL::StatusCode::SUCCESS;
      }
    }
  }
  if (mergedPassedPreviousCut) {
    h_cutflow->Fill(7);
    tvar_cutflow_merged_taus = 1;
    mergedPassedPreviousCut = 1;
  }

  // cutflow
  if (btaggedConeNonAssociatedTrackJets.size() != 0) mergedPassedPreviousCut = 0;
  if (mergedPassedPreviousCut) {
    h_cutflow->Fill(8);
    tvar_cutflow_merged_bJetVeto = 1;
    mergedPassedPreviousCut = 1;
  }

  // cutflow
{
  float HT_non_assoc = 0;
  float HT = 0;
  float HT_ratio = 1;
  if (leadingTrimmedFatJet) {
    for (auto smallJet : *smallJets) {
      if (smallJet->p4().DeltaR(leadingTrimmedFatJet->p4()) >= 1.0) HT_non_assoc += smallJet->pt();
    }
    HT = leadingTrimmedFatJet->pt() + HT_non_assoc;
    HT_ratio = HT_non_assoc / HT;
  }
  if (HT_ratio > 0.57) mergedPassedPreviousCut = 0;
  if (mergedPassedPreviousCut) {
    h_cutflow->Fill(9);
    tvar_cutflow_merged_HTcut = 1;
    mergedPassedPreviousCut = 1;
  }
}
  // cutflow
  // the last parts of the cutflow are binned and orthogonal - they are the 0, 1, and 2 b-tag regions
  if(mergedPassedPreviousCut) {
  if (tvar_ghostAssociatedTrackJet_btag_num == 0) {
    h_cutflow->Fill(10);
    tvar_cutflow_merged_0btag = 1;
  }
  if (tvar_ghostAssociatedTrackJet_btag_num == 1) {
    h_cutflow->Fill(11);
    tvar_cutflow_merged_1btag = 1;
  }
  if (tvar_ghostAssociatedTrackJet_btag_num == 2) {
    h_cutflow->Fill(12);
    tvar_cutflow_merged_2btag = 1;
  }
  }

  outTree->Fill();

  tvar_centralJet_pt.clear();
  tvar_centralJet_eta.clear();
  tvar_centralJet_phi.clear();
  tvar_centralJet_m.clear();
  tvar_forwardJet_pt.clear();
  tvar_forwardJet_eta.clear();
  tvar_forwardJet_phi.clear();
  tvar_forwardJet_m.clear();
  tvar_trimmedFatJet_pt.clear();

  return EL::StatusCode::SUCCESS;

}

EL::StatusCode xAHmonoHAlgo :: postExecute ()
{
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.

  return EL::StatusCode::SUCCESS;

}

EL::StatusCode xAHmonoHAlgo :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.

  ATH_MSG_INFO( "Deleting tool instances...");

  return EL::StatusCode::SUCCESS;

}

EL::StatusCode xAHmonoHAlgo :: histFinalize ()
{
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.

  return EL::StatusCode::SUCCESS;

}
