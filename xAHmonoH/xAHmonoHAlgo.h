#ifndef xAHmonoH_xAHmonoH_H
#define xAHmonoH_xAHmonoH_H

// EL include(s):
#include <EventLoop/StatusCode.h>
#include <EventLoop/Algorithm.h>

// EDM include(s):
#include <xAODEventInfo/EventInfo.h>
#include <xAODJet/JetContainer.h>
#include <xAODJet/JetAuxContainer.h>
#include <xAODBase/IParticle.h>
#include <xAODBase/IParticleContainer.h>

// Infrastructure include(s):
#include <xAODRootAccess/Init.h>
#include <xAODRootAccess/TEvent.h>
#include <xAODRootAccess/TStore.h>

// for the cluster and tracks information
#include "xAODJet/JetConstituentVector.h"
#include "xAODBTagging/BTagging.h"

// for retrieving of topocluster container
#include "xAODCaloEvent/CaloCluster.h"
#include "xAODCaloEvent/CaloClusterContainer.h"

// algorithm wrapper
#include <xAODAnaHelpers/Algorithm.h>
#include <xAODAnaHelpers/TreeAlgo.h>

// for event count
#include "xAODCutFlow/CutBookkeeper.h"
#include "xAODCutFlow/CutBookkeeperContainer.h"

#include "xAODJet/Jet.h"
#include "xAODJet/JetAttributes.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthParticleContainer.h"

// root includes
#include "TH1D.h"
#include "TH2D.h"
#include "TProfile.h"
#include "TTree.h"
#include <TFile.h>
#include <TLorentzVector.h>

class xAHmonoHAlgo : public TreeAlgo
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.

  // allpublic variables are configurable and do not have //!
  // all private and protected variables are not configurable and need //!

public:

  // configuration variables
  std::string m_name;
  std::string m_tree_name;
  std::string m_muonContainer_name;
  std::string m_electronContainer_name;
  std::string m_smallJet_name;
  std::string m_trimmedFatJet_name;
  std::string m_metContainer_name;
  std::string m_trackMetContainer_name; // this does not work
  std::string m_tauJet_name;
  std::string m_trackJet_name;

private:

  // event counter
  int m_eventCounter; //!
  TH1D *h_EventCounter; //!

  // histograms to save output
  TH1D *h_cutflow_resolved; //!
  TH1D *h_cutflow; //!
  TH1D *h_trimmedFatJet_pt; //!

  // TTree to save output results
  TTree *outTree;   //!

  bool tvar_cutflow_resolved_init; //!
  bool tvar_cutflow_resolved_lepton; //!
  bool tvar_cutflow_resolved_met_lt500; //!
  bool tvar_cutflow_resolved_met_gt150; //!
  bool tvar_cutflow_resolved_mpt_gt30; //!
  bool tvar_cutflow_resolved_mindphi_gt20; //!
  bool tvar_cutflow_resolved_dphimetmpt_lt90; //!
  bool tvar_cutflow_resolved_njetsCentral_gt2; //!

  bool tvar_cutflow_merged_init; //!
  bool tvar_cutflow_merged_lepton; //!
  bool tvar_cutflow_merged_met_gt500; //!
  bool tvar_cutflow_merged_mpt_gt30; //!
  bool tvar_cutflow_merged_mindphi_gt20; //!
  bool tvar_cutflow_merged_dphimetmpt_lt90; //!
  bool tvar_cutflow_merged_nfatjet_gt1; //!
  bool tvar_cutflow_merged_taus; //!
  bool tvar_cutflow_merged_bJetVeto; //!
  bool tvar_cutflow_merged_HTcut; //!
  bool tvar_cutflow_merged_0btag; //!
  bool tvar_cutflow_merged_1btag; //!
  bool tvar_cutflow_merged_2btag; //!

  int tvar_muon_num; //!
  int tvar_electron_num; //!
  int tvar_smallJet_btag_num; //!
  int tvar_ghostAssociatedTrackJet_btag_num; //!
  std::vector<double> tvar_centralJet_pt; //!
  std::vector<double> tvar_centralJet_eta; //!
  std::vector<double> tvar_centralJet_phi; //!
  std::vector<double> tvar_centralJet_m; //!
  std::vector<double> tvar_forwardJet_pt; //!
  std::vector<double> tvar_forwardJet_eta; //!
  std::vector<double> tvar_forwardJet_phi; //!
  std::vector<double> tvar_forwardJet_m; //!
  std::vector<double> tvar_trimmedFatJet_pt; //!
  double tvar_met_FinalTrk_met; //!
  double tvar_met_FinalTrk_phi; //!
  double tvar_met_RefJetTrk_met; //!
  double tvar_met_RefJetTrk_phi; //!

  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)

//FUNCTION DECLARATIONS
public:

  // this is a standard constructor
  xAHmonoHAlgo ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  // this is needed to distribute the algorithm to the workers
  ClassDef(xAHmonoHAlgo, 1);
};

#endif
